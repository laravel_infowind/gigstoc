<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gigstac Admin | </title>

    <!-- Bootstrap -->
    <link href="{{url('public/admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('public/admin/css/font-awesome.min.css')}}" rel="stylesheet">
    
    
    <!-- NProgress -->
    <link href="{{url('public/admin/css/nprogress.css')}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{url('public/admin/css/green.css')}}" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="{{url('public/admin/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{url('public/admin/css/jqvmap.min.css')}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{url('public/admin/css/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    
    <link href="{{url('public/admin/css/custom.min.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/css/toastr.css')}}" rel="stylesheet">
    <link href="{{url('public/admin/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    


    <script src="{{url('public/admin/js/jquery.min.js')}}"></script>
    
    <!-- Bootstrap -->
    <script src="{{url('public/admin/js/bootstrap.min.js')}}"></script>
    
    <script src="{{url('public/admin/js/jquery.dataTables.min.js')}}"></script>


    <!-- FastClick -->
    <script src="{{url('public/admin/js/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{url('public/admin/js/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{url('public/admin/js/Chart.min.js')}}"></script>
    <!-- gauge.js -->
    <script src="{{url('public/admin/js/gauge.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{url('public/admin/js/bootstrap-progressbar.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{url('public/admin/js/icheck.min.js')}}"></script>
    <!-- Skycons -->
    <script src="{{url('public/admin/js/skycons.js')}}"></script>
    <!-- Flot -->
    <script src="{{url('public/admin/js/jquery.flot.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.flot.pie.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.flot.time.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.flot.stack.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{url('public/admin/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{url('public/admin/js/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{url('public/admin/js/date.js')}}"></script>
    <!-- JQVMap -->
    <script src="{{url('public/admin/js/jquery.vmap.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.vmap.world.js')}}"></script>
    <script src="{{url('public/admin/js/jquery.vmap.sampledata.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{url('public/admin/js/moment.min.js')}}"></script>
    <script src="{{url('public/admin/js/daterangepicker.js')}}"></script>

    <!-- Custom Theme Scripts -->
  

    <script src="{{url('public/admin/js/toastr.js')}}"></script>
    
   
   
  </head>


  