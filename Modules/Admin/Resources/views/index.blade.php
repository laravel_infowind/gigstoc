@extends('admin::layouts.master')

@section('content')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<link href="{{url('public/admin/css/toastr.css')}}" rel="stylesheet">
<link href="{{url('public/admin/css/font-awesome.min.css')}}" rel="stylesheet">
<script src="{{url('public/admin/js/toastr.js')}}"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7 login-sec">
            <div class="login-head-logo">
                <img src=""></a>
            </div>
            <div class="card">
                <div class="card-header">Login</div>

                <div class="card-body">
				<form id="login-form" action="{{url('admin/login')}}" method="post" autocomplete="off">
             
                       
                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">UserName</label>
                            <div class="col-md-6">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input  type="text" name="email"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                            <div class="col-md-6">
                                <input  type="password" name="password"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input type="checkbox" id="login_checkbox" class="form-check-input" name="remember" {{ old('remember') || isset($emaill)? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                       Remember
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" id="login_button" class="btn btn-primary">
                                    Login
                                    <i id="login_loader" class="fa fa-spinner fa-spin" style="display:none"></i>
                                </button>
                            </div>
                        </div>
                    </form>
					{!! JsValidator::formRequest('App\Http\Requests\Admin\AdminLoginPostRequest','#login-form') !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script>

toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide"
        };

$("#login-form").submit(function(e) {

        e.preventDefault(); 
        var form = $(this);
        var url = form.attr('action');
        if ($("#login-form").valid()) {
            $('#login_button').prop('disabled',true);
            $('#login_loader').show();
            $.ajax({
            headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "POST",
            url: url,
            data: form.serialize(), 
            dataType:'json',
            success: function(data)
            {
                if (data.success){
                    toastr.success(data.message); 
                    window.location.href = "admin/dashboard";
                   
                } else {
                    toastr.error(data.message); 
                }
                $('#login_loader').hide();
                $('#login_button').prop('disabled',false);
            }
            });
        }

});

</script>

@endsection
