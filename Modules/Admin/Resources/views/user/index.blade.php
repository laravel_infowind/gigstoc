@extends('admin::layouts.inner-master')
@section('content')
<div class="right_col" role="main">
    <h1>User list</h1>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
   
</body>
   
<script type="text/javascript">
  $(function () {
      
     var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        oLanguage: {
            zeroRecords: "Nothing found",
            infoEmpty: "No records available",
            sProcessing:'<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: "{{ url('admin/user-list') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'fullname', name: 'fullname', searchable: true},
            {data: 'email', name: 'email', searchable: true},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        "order": [[ 1, 'asc' ]]
    });
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    
  });
  </script>

  @endsection