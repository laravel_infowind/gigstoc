<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Requests\Admin\AdminLoginPostRequest;
use Illuminate\Support\Facades\Auth;
use Response;
use App\User;
use DataTables;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    public function dashboard()
    {
        return view('admin::dashboard.index');
    }

    public function login(AdminLoginPostRequest $request)
    {
        
        $user = User::where(['email'=>trim($request->email)])->first();
        $credentials = ['email'=>trim($request->email),'password'=>$request->password];
        if (Auth::attempt($credentials)) {
            return Response::json(['success'=>true,'message' => 'Login successfully.']);
        }
        return Response::json(['success'=>false,'message' => 'Invalid Dredential.']);
    }

    public function logout()
    {
        Auth::logout();
        \Session::flush();
        return redirect('/admin');
    }

    public function user(Request $request){
        return view('admin::user.index');
    }

    public function userList(Request $request)
    {
        try{
            if ($request->ajax()) {
                        $data = User::with(['userInRole.role'])->latest();
                        $data->whereHas('userInRole.role', function($q)
                        {
                            $q->where('name','!=', 'admin');    
                        });
                        $data = $data->get();
                    
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                            $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
        
                                return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
            }
        }catch(Exception $ex){
            print_r($ex->getMessage());die;
        }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
