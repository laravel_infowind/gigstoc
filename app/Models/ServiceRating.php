<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ServiceRating extends Model
{
   
    protected $table = 'service_ratings';

    protected $fillable = [
        'service_id', 'user_id', 'rating','review','is_published'
    ];
   
 
   
}
