<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TagInJobRequest extends Model
{
   
    protected $table = 'tag_in_job_requests';

    protected $fillable = [
        'job_request_id', 'tag_id'
    ];
   
 
   
}
