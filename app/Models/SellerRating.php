<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SellerRating extends Model
{
   
    protected $table = 'seller_ratings';

    protected $fillable = [
        'from_id', 'to_id', 'rating','review','is_published'
    ];
   
 
   
}
