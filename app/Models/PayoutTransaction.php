<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PayoutTransaction extends Model
{
   
    protected $table = 'payout_transactions';

    protected $fillable = [
        'sender_id', 'receiver_id', 'amount','transaction_id','payment_type','status'
    ];
   
 
   
}
