<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BookMark extends Model
{
   
    protected $table = 'bookmarks';

    protected $fillable = [
        'user_id', 'service_id', 'is_bookmarked'
    ];
   
 
   
}
