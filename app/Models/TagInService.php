<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagInService extends Model
{
    
    protected $table = 'tag_in_services';

    protected $fillable = [
        'service_id','tag_id'
    ];
   
}
