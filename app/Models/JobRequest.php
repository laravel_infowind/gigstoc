<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class JobRequest extends Model
{
   
    protected $table = 'job_requests';

    protected $fillable = [
        'title', 'description', 'budget','deadline','category_id','created_by','is_completed'
    ];
   
 
   
}
