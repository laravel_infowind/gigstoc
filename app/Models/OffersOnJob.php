<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class OffersOnJob extends Model
{
   
    protected $table = 'offers_on_job';

    protected $fillable = [
        'job_request_id', 'service_id', 'requested_by'
    ];
   
 
   
}
