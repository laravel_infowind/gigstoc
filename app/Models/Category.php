<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
   
    protected $table = 'categories';

    protected $fillable = [
        'name', 'slug', 'description','cat_image','parent_cat_id','created_by','is_featured','is_active','visible_in_menu'
    ];
   
 
   
}
