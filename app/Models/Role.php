<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
   
   protected $table='roles';

   protected $fillable = [
    'name', 'is_active'
   ];
   
    public function role(){
        return $this->belongsTo('App\Models\Role','role_id')->select('name');
    }
   

}
