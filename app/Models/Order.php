<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
   
    protected $table = 'orders';

    protected $fillable = [
        'order_id', 'user_id', 'subtotal','tax','total','status','transaction_id','payment_type','payment_history','cart_data'
    ];
   
 
   
}
