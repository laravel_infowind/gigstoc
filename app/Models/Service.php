<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    
    protected $table = 'services';

    protected $fillable = [
        'title','price','deadline','category_id','description','is_video','video_link','video_type','views','is_custom','created_by','parent_service_id','is_active'
    ];
   
}
