<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInRole extends Model
{
   
    protected $table = 'user_in_roles';

    protected $fillable = [
        'role_id', 'user_id'
    ];
   
    public function role(){
        return $this->belongsTo('App\Models\Role', 'role_id')->select('name');
    }
}

